package com.jenkins.pipeline

import static org.junit.Assert.*

import org.junit.Test

import com.jenkins.pipeline.BranchSpecification
import com.jenkins.pipeline.BranchStrategy

class BranchStrategyTest extends BranchStrategy {
  
  private void test_findBranchSpec(List<BranchSpecification> strategy, List asserts) {
    def BranchSpecification spec
    asserts.each {
      spec = BranchStrategy.findBranchSpec(it.name, strategy)
      assertTrue("assert branch name '${it.name}' -> branch spec '${it.label}'", spec ? spec.label == it.label : !it.label)
    }
  }

  @Test
  public void test_findBranchSpec_fxfStandardStrategy() {
    def List<BranchSpecification> strategy = new BranchStrategy().fxfStandardStrategy

    def List asserts =
        [
          /*
           * Assert Map Definition:
           *   name:  branch name being tested
           *   label: expected label of returned branch spec; empty string means no match is expected 
           */
          [name: 'story_123',                label: ''],
          [name: 'dev',                      label: ''],
          [name: '1.0_develop',              label: ''],
          [name: 'branches/development',     label: ''],
          [name: '1.0_trunk',                label: ''],

          [name: 'develop',                  label: 'fxf-std-dev'],
          [name: 'Develop',                  label: 'fxf-std-dev'],
          [name: 'development',              label: 'fxf-std-dev'],
          [name: 'develop-1.0.0',            label: 'fxf-std-dev'],
          // develop matches before release
          [name: 'develop/release-1.0',      label: 'fxf-std-dev'],

          [name: 'trunk',                    label: 'fxf-std-dev'],
          [name: 'trunk/project_a',          label: 'fxf-std-dev'],
          // trunk matches before release
          [name: 'trunk/release_1.0',        label: 'fxf-std-dev'],

          [name: 'FEATURE',                  label: 'fxf-std-feature'],
          [name: 'feature/desc_123',         label: 'fxf-std-feature'],
          [name: 'story_1-feature',          label: 'fxf-std-feature'],
          [name: 'branches/feature_123',     label: 'fxf-std-feature'],
          // feature matches before develop
          [name: 'develop/feature-desc_123', label: 'fxf-std-feature'],
          // feature matches before release
          [name: 'branches/release_1.0/feature_123', label: 'fxf-std-feature'],
          // feature matches before trunk and release
          [name: 'trunk/release_1.0/feature_123',     label: 'fxf-std-feature'],

          [name: 'master',                   label: 'fxf-std-master'],
          [name: 'Master',                   label: 'fxf-std-master'],
          [name: 'branches/master',          label: 'fxf-std-master'],
          [name: 'master_branch',            label: 'fxf-std-master'],

          [name: 'production',               label: 'fxf-std-master'],
          [name: 'Production',               label: 'fxf-std-master'],
          [name: '/branches/production',     label: 'fxf-std-master'],
          [name: 'production_branch',        label: 'fxf-std-master'],

          [name: 'release',                  label: 'fxf-std-release'],
          [name: 'Release',                  label: 'fxf-std-release'],
          [name: '1.0.0_release',            label: 'fxf-std-release'],
          [name: 'branches/release_1.0',     label: 'fxf-std-release'],

          [name: 'hotfix',                   label: 'fxf-std-release'],
          [name: 'HotFix',                   label: 'fxf-std-release'],
          [name: 'HotFix_1.0.0-0-1',         label: 'fxf-std-release'],
          [name: '1.0.0-HotFix_1',           label: 'fxf-std-release'],
          [name: 'branches/1.0.0_Hotfix_1',  label: 'fxf-std-release'],
          [name: 'branches/release_1.0/hotfix_1', label: 'fxf-std-release'],
        ]

    test_findBranchSpec(strategy, asserts)
  }

  @Test
  public void test_findBranchSpec_fxfDefaultStrategy() {
    def List<BranchSpecification> strategy = new BranchStrategy().fxfDefaultStrategy

    def List asserts =
        [
          /*
           * Assert Map Definition:
           *   name:  branch name being tested
           *   label: expected label of returned branch spec; empty string means no match is expected 
           */
          [name: 'fxfDefaultStrategy-Matches_Any/branch.name', label: 'fxf-default-any'],
        ]

    test_findBranchSpec(strategy, asserts)
  }
}

