package com.jenkins.pipeline

import com.cloudbees.groovy.cps.NonCPS // Resolves NonCPS annotation when using standard groovy compiler.

class BranchStrategy implements Serializable {

  def fxfDefaultStrategy = [new BranchSpecification(label: 'fxf-default-any', regex: '.+', activeStages: Constants.MASK_STAGE_ALL & ~Constants.MASK_STAGE_FORTIFY, fortify_cron_job: true)]

  def fxfStandardStrategy = [
    new BranchSpecification(label: 'fxf-std-feature', regex: 'feature', activeStages: Constants.MASK_STAGE_ALL & ~Constants.MASK_STAGE_FORTIFY & ~Constants.MASK_STAGE_DOWNSTREAM & ~Constants.MASK_STAGE_DEPLOY & ~Constants.MASK_STAGE_NEXUS),
    new BranchSpecification(label: 'fxf-std-dev', regex: '^(develop|trunk)', activeStages: Constants.MASK_STAGE_ALL & ~Constants.MASK_STAGE_FORTIFY, fortify_cron_job: true),
    new BranchSpecification(label: 'fxf-std-master', regex: '(master|production)', activeStages: Constants.MASK_STAGE_ALL & ~Constants.MASK_STAGE_FORTIFY & ~Constants.MASK_STAGE_DOWNSTREAM & ~Constants.MASK_STAGE_DEPLOY & ~Constants.MASK_STAGE_NEXUS),
    new BranchSpecification(label: 'fxf-std-release', regex: '(release|hotfix)', activeStages: Constants.MASK_STAGE_ALL & ~Constants.MASK_STAGE_FORTIFY & ~Constants.MASK_STAGE_DOWNSTREAM, fortify_cron_job: true)
  ]

  /*
   * Sequentially searches branchSpecification list representing the active branch strategy
   * for first spec that matches the branchName and returns it. If no match is found,
   * null is returned. 
   */
  @NonCPS
  public static BranchSpecification findBranchSpec(String branchName, List<BranchSpecification> branchStrategy) {
    def lc_branchName = branchName.toLowerCase()
    def branchSpec = branchStrategy.find {
      lc_branchName =~ it.regex
    }
    return branchSpec
  }
}
