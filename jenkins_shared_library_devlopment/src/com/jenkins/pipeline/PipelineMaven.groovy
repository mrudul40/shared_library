package com.jenkins.pipeline

import com.jenkins.pipeline.*
import groovy.transform.InheritConstructors

@InheritConstructors
class PipelineMaven extends PipelineTemplate {

  def stageBuild() {        
    bat "'${pipeProperties.mvnHome}/bin/mvn' -f ${pipeProperties.projectDirectory}/pom.xml clean package site versions:display-dependency-updates versions:dependency-updates-report versions:display-plugin-updates versions:plugin-updates-report -s ${pipeProperties.projectDirectory}/settings.xml"
    archiveArtifacts(config.artifacts_to_archive ?: pipeProperties.buildProperties.patterns.artifacts)
    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: '${pipeProperties.projectDirectory}/target/site', reportFiles: 'dependency-updates-report.html', reportName: 'Outdated Dependency', reportTitles: ''])
  }

  def stageSonarQube() {
    // withSonarQubeEnv() wraps call to this method and provides the plugin's global sonar.host.url and sonar.login settings as env vars
    bat "'${pipeProperties.mvnHome}/bin/mvn' -f ${pipeProperties.projectDirectory}/pom.xml sonar:sonar -Dsonar.host.url=\$SONAR_HOST_URL -Dsonar.login=\$SONAR_AUTH_TOKEN -Dmaven.test.skip=true -s ${pipeProperties.projectDirectory}/settings.xml"
  }

  def stageNexus() {
    bat "'${pipeProperties.mvnHome}/bin/mvn' -f ${pipeProperties.projectDirectory}/pom.xml deploy -Dmaven.test.skip=true -s ${pipeProperties.projectDirectory}/settings.xml"
  }
}
