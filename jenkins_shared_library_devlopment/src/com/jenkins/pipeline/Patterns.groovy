package com.jenkins.pipeline

class Patterns implements Serializable {

  def mavenJava = [
    jacocoClass: '**/classes',
    jacocoExec: '**/**.exec',
    jacocoSource: '**/src/main/java',
    findbugs: '**/findbugsXml.xml',
    junitTestResults: '**/target/surefire-reports/**/*.xml',
    pmd: '**/pmd.xml',
    artifacts: '**/target/*.war, **/target/*.jar, **/target/*.ear'
  ]

  def gradleJava = [
    jacocoClass: '**/classes',
    jacocoExec: '**/*.exec',
    jacocoSource: '**/src/main/java',
    findbugs: '**/reports/**/findbugs.xml',
    junitTestResults: '**/TEST*.xml',
    pmd: '**/reports/**/pmd.xml',
    artifacts: '**/build/libs/*.jar'
  ]

  def gradleAndroidRelease = [
    jacocoClass: '**/classes/release',
    jacocoExec: '**/*Release*.exec',
    jacocoSource: '**/src/main/java',
    findbugs: '**/findbugs.xml',
    junitTestResults: '**/TEST*.xml',
    pmd: '**/pmd.xml',
    artifacts: '**/outputs/apk/**/*.apk'
  ]
}