package com.jenkins.pipeline

class BranchSpecification implements Serializable {
  def label = 'none' // Informational label used to uniquely describe each branch spec in a branch strategy.
  def regex = ''
  def activeStages = Constants.MASK_STAGE_NONE
  def fortify_cron_job = false
}