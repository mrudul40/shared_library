package com.jenkins.pipeline

import com.jenkins.pipeline.*
import groovy.transform.InheritConstructors

@InheritConstructors
abstract class PipelineTemplate extends PipelineBase {

  def executePipeline() {

    stage('Build', pipeProperties.activeStages & Constants.MASK_STAGE_BUILD) {
      withCredentials(pipeProperties.credentials) { stageBuild() }
    }

    parallel 'SonarQube': {
      withSonarQubeEnv('SonarQube') {
        stage('SonarQube Analysis', pipeProperties.activeStages & Constants.MASK_STAGE_SONARQUBE) { stageSonarQube() }
      }
    },
    'JUnit': {
      stage('JUnit', pipeProperties.activeStages & Constants.MASK_STAGE_JUNIT) { stageJUnit() }
    },
    'JaCoCo': {
      stage('JaCoCo', pipeProperties.activeStages & Constants.MASK_STAGE_JACOCO) { stageJaCoCo() }
    },
    'Findbugs': {
      stage('Findbugs', pipeProperties.activeStages & Constants.MASK_STAGE_FINDBUGS) { stageFindbugs() }
    },
    'PMD': {
      stage('PMD', pipeProperties.activeStages & Constants.MASK_STAGE_PMD) { stagePMD() }
    }

    stage('Nexus Upload', pipeProperties.activeStages & Constants.MASK_STAGE_NEXUS) {
      if (getCurrentResult() != 'FAILURE' && getCurrentResult() != 'UNSTABLE') {
        withCredentials(pipeProperties.credentials) { stageNexus() }
      }
    }

    stage('Deploy', (config.udeploy || config.pcf) && (pipeProperties.activeStages & Constants.MASK_STAGE_DEPLOY)) {
      if (config.udeploy) {
        stageUDeploy()
      }
      else if (config.pcf) {
        stagePCFDeploy()
      }
    }

    stage('Trigger Builds', pipeProperties.activeStages & Constants.MASK_STAGE_DOWNSTREAM) { stageDownstream() }

    stage('Fortify Analysis', pipeProperties.activeStages & Constants.MASK_STAGE_FORTIFY) { stageFortify() }
  }

  abstract stageBuild()

  abstract stageSonarQube()

  def stageJUnit() {
    junit(healthScaleFactor: config.junit_healthScaleFactor ?: 1.0, testResults: pipeProperties.buildProperties.patterns.junitTestResults)
  }

  def stageJaCoCo() {
    step([$class: 'JacocoPublisher', classPattern: pipeProperties.buildProperties.patterns.jacocoClass, execPattern: pipeProperties.buildProperties.patterns.jacocoExec, sourcePattern: pipeProperties.buildProperties.patterns.jacocoSource, changeBuildStatus: true, maximumBranchCoverage: config.jacoco_maximumBranchCoverages ?: '', maximumClassCoverage: config.jacoco_maximumClassCoverage ?: '', maximumComplexityCoverage: config.jacoco_maximumComplexityCoverage ?: '', maximumInstructionCoverage: config.jacoco_maximumInstructionCoverage ?: '', maximumLineCoverage: config.jacoco_maximumLineCoverage ?: '', maximumMethodCoverage: config.jacoco_maximumMethodCoverage ?: '', minimumBranchCoverage: config.jacoco_minimumBranchCoverage ?: '', minimumClassCoverage: config.jacoco_minimumClassCoverage ?: '', minimumComplexityCoverage: config.jacoco_minimumComplexityCoverage ?: '', minimumInstructionCoverage: config.jacoco_minimumInstructionCoverage ?: '', minimumLineCoverage: config.jacoco_minimumLineCoverage ?: '', minimumMethodCoverage: config.jacoco_minimumMethodCoverage ?: ''])
  }

  def stageFindbugs() {
    step([$class: 'FindBugsPublisher', pattern: config.findbugs_pattern ?: pipeProperties.buildProperties.patterns.findbugs, excludePattern: config.findbugs_excludePattern ?: '', includePattern: config.findbugs_includePattern ?: '', defaultEncoding: config.defaultEncoding ?: '', canRunOnFailed: config.findbugs_canRunOnFailed ?: false, shouldDetectModules: config.findbugs_shouldDetectModules ?: true, healthy: config.findbugs_healthy ?: '', unHealthy: config.findbugs_unHealthy ?: '', thresholdLimit: config.findbugs_thresholdLimit ?: '', canComputeNew: config.findbugs_canComputeNew ?: true, unstableTotalAll: config.findbugs_unstableTotalAll ?: '', unstableTotalHigh: config.findbugs_unstableTotalHigh ?: '', unstableTotalNormal: config.findbugs_unstableTotalNormal ?: '', unstableTotalLow: config.findbugs_unstableTotalLow ?: '', unstableNewAll: config.findbugs_unstableNewAll ?: '', unstableNewHigh: config.findbugs_unstableNewHigh ?: '', unstableNewNormal: config.findbugs_unstableNewNormal ?: '', unstableNewLow: config.findbugs_unstableNewLow ?: '', failedTotalAll: config.findbugs_failedTotalAll ?: '', failedTotalHigh: config.findbugs_failedTotalHigh ?: '', failedTotalNormal: config.findbugs_failedTotalNormal ?: '', failedTotalLow: config.findbugs_failedTotalLow ?: '', failedNewAll: config.findbugs_failedNewAll ?: '', failedNewHigh: config.findbugs_failedNewHigh ?: '', failedNewNormal: config.findbugs_failedNewNormal ?: '', failedNewLow: config.findbugs_failedNewLow ?: ''])
  }

  def stagePMD() {
    step([$class: 'PmdPublisher', pattern: config.pmd_pattern ?: pipeProperties.buildProperties.patterns.pmd, defaultEncoding: config.defaultEncoding ?: '', canRunOnFailed: config.pmd_canRunOnFailed ?: false, shouldDetectModules: config.pmd_shouldDetectModules ?: true, healthy: config.pmd_healthy ?: '', unHealthy: config.pmd_unHealthy ?: '', thresholdLimit: config.pmd_thresholdLimit ?: '', canComputeNew: config.pmd_canComputeNew ?: true, unstableTotalAll: config.pmd_unstableTotalAll ?: '', unstableTotalHigh: config.pmd_unstableTotalHigh ?: '', unstableTotalNormal: config.pmd_unstableTotalNormal ?: '', unstableTotalLow: config.pmd_unstableTotalLow ?: '', unstableNewAll: config.pmd_unstableNewAll ?: '', unstableNewHigh: config.pmd_unstableNewHigh ?: '', unstableNewNormal: config.pmd_unstableNewNormal ?: '', unstableNewLow: config.pmd_unstableNewLow ?: '', failedTotalAll: config.pmd_failedTotalAll ?: '', failedTotalHigh: config.pmd_failedTotalHigh ?: '', failedTotalNormal: config.pmd_failedTotalNormal ?: '', failedTotalLow: config.pmd_failedTotalLow ?: '', failedNewAll: config.pmd_failedNewAll ?: '', failedNewHigh: config.pmd_failedNewHigh ?: '', failedNewNormal: config.pmd_failedNewNormal ?: '', failedNewLow: config.pmd_failedNewLow ?: ''])
  }

  abstract stageNexus()

  def stageDownstream() {
    (config.downstream_jobs ?: []).each {
      triggerRemoteJob(remotePathMissing: [$class: 'StopAsFailure'], remotePathUrl: "${it}")
    }
  }

  def stagePCFDeploy() {
    def appServiceAccountLibrary = library('AppServiceAccount').com.fedex.jenkins

    sh '''#!/bin/bash
          export https_proxy=http://internet.proxy.fedex.com:3128
          curl -L "https://cli.run.pivotal.io/stable?release=linux64-binary&source=github" --fail | tar -zx
          chmod +x cf
       '''

    withEnv(["PATH=${env.path}:${env.workspace}"]) {
      if (config.pcf.pamId) {
        def appStract = appServiceAccountLibrary.APPIDextractor.new()
        def k = appServiceAccountLibrary.PamConnection.new()
        def pcfPassword = k.pcfDeploy(config.pcf.pamId)
        def appId = appStract.appIDParser(env.GIT_URL)
        def appIdNum = appId.substring(3)
        sh("cf login -a '${config.pcf.url}' -u 'FXS_${appId}' -p '${pcfPassword}' -o '${appIdNum}' -s '${config.pcf.space}'")
      }

      withCredentials(config.pcf.credentials ?: []) { sh config.pcf.script }
    }
  }

  def stageUDeploy() {
    def artifactVersion = sh (script: "'${pipeProperties.mvnHome}/bin/mvn' -q -f ${pipeProperties.projectDirectory}/pom.xml exec:exec -Dexec.executable='echo' -Dexec.args='\${project.version}' --non-recursive -s ${pipeProperties.projectDirectory}/settings.xml", returnStdout: true).trim()
    if (artifactVersion.contains("SNAPSHOT")) {
      def timestamp = sh(script: "date +%Y%m%d.%H%M%S", returnStdout: true).trim()
      artifactVersion = "${artifactVersion}-${timestamp}-${env.BUILD_NUMBER}"
    }

    withCredentials([usernamePassword(credentialsId: "${config.udeploy['credentials_id']}" , passwordVariable: 'DEPLOY_PASSWORD', usernameVariable: 'DEPLOY_USER')]) {
      def uDeploySteps = [
        $class: 'UCDeployPublisher',
        siteName: "Enterprise UrbanCode Instance 1",
        altUser: [
          adminUser: false,
          altPassword: hudson.util.Secret.fromString(env.DEPLOY_PASSWORD),
          altUsername: env.DEPLOY_USER
        ],
        component:[
          $class: 'com.urbancode.jenkins.plugins.ucdeploy.VersionHelper$VersionBlock',
          componentName: "${config.udeploy['componentName']}",
          delivery: [
            $class: 'com.urbancode.jenkins.plugins.ucdeploy.DeliveryHelper$Push',
            pushVersion: "${artifactVersion}",
            baseDir: "${env.workspace}/${config.udeploy['artifactDirectory']}",
            fileIncludePatterns: "${config.udeploy['artifactFile']}",
            fileExcludePatterns: '',
            pushDescription: 'Jenkins Generated'
          ]
        ]
      ]

      if (config.udeploy['deployEnvironment']) {
        uDeploySteps << [
          deploy: [
            $class: 'com.urbancode.jenkins.plugins.ucdeploy.DeployHelper$DeployBlock',
            deployApp: "${config.udeploy['deployApp'] ?: config.udeploy['componentName']}",
            deployEnv: "${config.udeploy['deployEnvironment']}",
            deployProc: "${config.udeploy['deployProcess']}",
            createProcess: [
              $class: 'com.urbancode.jenkins.plugins.ucdeploy.ProcessHelper$CreateProcessBlock',
              processComponent: 'Deploy'
            ],
            deployVersions: "${config.udeploy['componentName']}:${artifactVersion}",
            deployOnlyChanged: false
          ]
        ]
      }

      step(uDeploySteps)
    }
  }

  def stageFortify() {
    if (!config.fortify_id) {
      abortBuild('Fortify requested but fortify_id not specified.')
    }
    node('master') {
      dir(env.JENKINS_HOME + '/userContent/fortify/FXF') {
        stash name:'stash-fortify', includes:'fortifyFunctions.sh, translate.sh'
      }
    }
    unstash 'stash-fortify'
    sh "chmod a+x ${env.workspace}/*.sh"
    sh "${env.workspace}/translate.sh ${config.fortify_id} RUN"
  }
}
