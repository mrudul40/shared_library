package com.jenkins.pipeline

import com.fedex.fxf.*
import groovy.transform.InheritConstructors

@InheritConstructors
class PipelineGradle extends PipelineTemplate {

  def stageBuild() {
    sh "chmod a+x ${pipeProperties.projectDirectory}/gradlew && '${pipeProperties.projectDirectory}/gradlew' --no-daemon clean build check findbugs pmd jacoco -b ${pipeProperties.projectDirectory}/build.gradle"
    archiveArtifacts(config.artifacts_to_archive ?: pipeProperties.buildProperties.patterns.artifacts)
  }

  def stageSonarQube() {
    // withSonarQubeEnv() wraps call to this method and provides the plugin's global sonar.host.url and sonar.login settings as env vars
    // --info required due to this bug until Jenkins SonarQube plugin 2.7 is released:  https://jira.sonarsource.com/browse/SONARJNKNS-281
    sh "chmod a+x ${pipeProperties.projectDirectory}/gradlew && '${pipeProperties.projectDirectory}/gradlew' --no-daemon --info sonarqube -Dsonar.host.url=\$SONAR_HOST_URL -Dsonar.login=\$SONAR_AUTH_TOKEN -b ${pipeProperties.projectDirectory}/build.gradle"
  }

  def stageNexus() {
    def String gradleTasks = sh(script: "chmod a+x ${pipeProperties.projectDirectory}/gradlew && '${pipeProperties.projectDirectory}/gradlew' --no-daemon tasks -b ${pipeProperties.projectDirectory}/build.gradle", returnStdout: true).trim()
    def String task = (gradleTasks ==~ /(?ms).*Publishing tasks.*/) ? 'publish' : 'upload'
    sh "'${pipeProperties.projectDirectory}/gradlew' --no-daemon ${task} -b ${pipeProperties.projectDirectory}/build.gradle"
  }
}