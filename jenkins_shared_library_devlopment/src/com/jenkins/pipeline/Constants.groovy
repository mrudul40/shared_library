package com.jenkins.pipeline

class Constants {
  public static final MASK_STAGE_NONE =        0b0000000000000000
  public static final MASK_STAGE_BUILD =       0b0000000000000010
  public static final MASK_STAGE_SONARQUBE =   0b0000000000000100
  public static final MASK_STAGE_JUNIT =       0b0000000000001000
  public static final MASK_STAGE_JACOCO =      0b0000000000010000
  public static final MASK_STAGE_FINDBUGS =    0b0000000000100000
  public static final MASK_STAGE_PMD =         0b0000000010000000
  public static final MASK_STAGE_NEXUS =       0b0000000100000000
  public static final MASK_STAGE_DOWNSTREAM =  0b0000001000000000
  public static final MASK_STAGE_DEPLOY =      0b0000010000000000
  public static final MASK_STAGE_FORTIFY =     0b0000100000000000
  public static final MASK_STAGE_ALL =         0b1111111111111111
}