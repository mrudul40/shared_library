package com.jenkins.pipeline

abstract class PipelineBase implements Serializable {

  def script
  def config
  def env
  def params
  def scm
  def pipeProperties

  def PipelineBase(script, config, pipeProperties) {
    this.script = script
    this.config = config
    this.pipeProperties = pipeProperties
    this.env = script.env
    this.params = script.params
    this.scm = script.scm
  }

  def abortBuild(message) {
    script.currentBuild.result = 'ABORTED'
    script.error(message)
  }

  def archiveArtifacts(args) {
    return script.archiveArtifacts(args)
  }

  def checkout(scm) {
    return script.checkout(scm)
  }

  def cleanWs(args) {
    return script.cleanWs(args)
  }

  def dir(path, closure) {
    return script.dir(path, closure)
  }

  def echo(message) {
    return script.echo(message)
  }

  def emailext(args) {
    return script.emailext(args)
  }

  def getCurrentResult() {
    return script.currentBuild.currentResult
  }

  def junit(args) {
    return script.junit(args)
  }

  def library(libName) {
    return script.library(libName)
  }

  def mattermostSend(args) {
    return script.mattermostSend(args)
  }

  def node(label, closure) {
    return script.node(label, closure)
  }

  def parallel(closure) {
    return script.parallel(closure)
  }

  def parameterizedCron(expression) {
    return script.parameterizedCron(expression)
  }

  def pcfDeploy(config) {
    return script.pcfDeploy(config)
  } 

  def pipelineTriggers(triggers) {
    return script.pipelineTriggers(triggers)
  }

  def properties(properties) {
    return script.properties(properties)
  }

  def setBuildResult(result) {
    script.currentBuild.result = result
  }

  def sh(command) {
    return script.sh(command)
  }

  def stage(name, execute, closure) {
    return script.stage(name, execute ? closure : { script.echo "Skipped Stage:  $name" })
  }

  def stash(args) {
    return script.stash(args)
  }

  def step(steps) {
    return script.step(steps)
  }

  def tool(name) {
    return script.tool(name)
  }

  def triggerRemoteJob(args) {
    return script.triggerRemoteJob(args)
  }

  def unstash(args) {
    return script.unstash(args)
  }

  def usernamePassword(credentials) {
    return script.usernamePassword(credentials)
  }

  def withCredentials(credentials, closure) {
    return script.withCredentials(credentials, closure)
  }

  def withEnv(vars, closure) {
    return script.withEnv(vars, closure)
  }

  def withSonarQubeEnv(environment, closure) {
    return script.withSonarQubeEnv(environment, closure)
  }
}