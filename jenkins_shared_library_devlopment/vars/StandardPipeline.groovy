import com.jenkins.pipeline.*

def call(body) {

  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  node("${config.node_label ?: ''}") {
    try {
      config.nexusCredentials = config.nexusCredentials ?: [:]
      config.nexusCredentials.id = config.nexusCredentials.id ?: '00075248-be90-4573-a94d-1ee40421acd1' // Default: FXF_Common_admin
      config.discardBuilds = config.discardBuilds ?: [:]
      env.JAVA_HOME = tool("${config.java_tool ?: 'JAVA_8'}")
      pipeProperties = [
        mvnHome: tool("${config.maven_tool ?: 'Maven 3.3.9'}"),
        projectDirectory: "${config.project_directory ?: '.'}",
        activeStages: Constants.MASK_STAGE_NONE,
        buildProperties: config.build_properties ?: [tool: 'maven', patterns: new Patterns().mavenJava]
      ]
      def triggers = config.cron_string ? [cron("${config.cron_string}")]: []
      def branchStrategy = config.branchStrategy ?: new BranchStrategy().fxfDefaultStrategy

      if (params.run_fortify_only) {
        pipeProperties.activeStages = Constants.MASK_STAGE_FORTIFY
      }
      else {
        def BranchSpecification branchSpec
        branchSpec = BranchStrategy.findBranchSpec(env.BRANCH_NAME, branchStrategy)
        if (!branchSpec) {
          currentBuild.result = 'ABORTED'
          error("Branch '${env.BRANCH_NAME}' did not match any branch name pattern specified in branching strategy.")
        }
        echo "Matched branch specification '${branchSpec.label}', branch name pattern (case-insensitive): '${branchSpec.regex}'"
        pipeProperties.activeStages = branchSpec.activeStages
        if (config.fortify_id && branchSpec.fortify_cron_job) {
          triggers += parameterizedCron('H H * * H %run_fortify_only=true')
        }
      }
      echo "activeStages = ${pipeProperties.activeStages}"

      def propertiesList = [
        pipelineTriggers(triggers),
        [$class: 'ParametersDefinitionProperty', parameterDefinitions: [[$class: 'BooleanParameterDefinition', name: 'run_fortify_only', defaultValue: false]]],
        [$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', daysToKeepStr: "${config.discardBuilds.daysToKeep ?: ''}", numToKeepStr: "${config.discardBuilds.maxNumberToKeep ?: ''}"]]
      ]
      if (pipeProperties.buildProperties.disableConcurrentBuilds) {
        propertiesList.add(disableConcurrentBuilds())
      }
      properties(propertiesList)

      stage('Source Checkout') {
        scmVars = checkout(scm)
        env.GIT_URL = scmVars.GIT_URL  // Needed by reference-pipeline/vars/pcfDeploy.groovy
      }

      switch (pipeProperties.buildProperties.tool.toLowerCase()) {
        case ~/^maven$/:
          pipeProperties.credentials = [usernamePassword(credentialsId: config.nexusCredentials.id, passwordVariable: config.nexusCredentials.passwordVariable ?: 'NexusPassword', usernameVariable: config.nexusCredentials.usernameVariable ?: 'NexusUser')]
          pipeline = new PipelineMaven(this, config, pipeProperties)
          break
        case ~/^gradle$/:
          pipeProperties.credentials = [usernamePassword(credentialsId: config.nexusCredentials.id, passwordVariable: config.nexusCredentials.passwordVariable ?: 'ORG_GRADLE_PROJECT_nexusPassword', usernameVariable: config.nexusCredentials.usernameVariable ?: 'ORG_GRADLE_PROJECT_nexusUsername')]
          pipeline = new PipelineGradle(this, config, pipeProperties)
          break
        default:
          currentBuild.result = 'ABORTED'
          error("Jenkinsfile configuration error; build_properties.tool value '${pipeProperties.buildProperties.tool}' is invalid")
          break
      }

      pipeline.executePipeline()
    }
    catch(err) {
      currentBuild.result = 'FAILED'
      echo 'Caught Exception:'
      echo err.message
      throw err
    }
    finally {
      emailext(attachLog: true, mimeType: 'text/html', body: "<a href='${env.BUILD_URL}'>${env.BUILD_URL}</a>", subject: "Build ${env.BUILD_NUMBER} ${currentBuild.currentResult} for ${env.JOB_NAME}", to: config.email_on_result ?: '')

      if (config.mattermost) {
        mattermostSend(
            message: "${currentBuild.currentResult}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})",
            endpoint: config.mattermost.endpoint,
            channel: config.mattermost.channel,
            color: currentBuild.currentResult == 'SUCCESS' ? '#00FF00' : currentBuild.currentResult == 'UNSTABLE' ? '#FFFF00' : '#FF0000')
      }

      cleanWs(notFailBuild: true)
    }
  }
}
